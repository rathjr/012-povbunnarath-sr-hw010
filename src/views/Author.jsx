import React, { useEffect, useState } from 'react'
import { deleteAuthor, fetchAuthor, postAuthor, updateAuthor, uploadImage, viewAuthor } from '../service/ServiceArthor'
import { Button, Table, Container, Row, Col,Form} from 'react-bootstrap'
import query from 'query-string'
import { useHistory, useLocation } from 'react-router'
export default function Author() {
    const [author, setAuthor] = useState([])
    const[name,setName]=useState('')
    const[email,setEmai]=useState('')
    const[imageUrl,setimageUrl]=useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const[imageFile,setimageFile]=useState(null)

    const {search}=useLocation()
    let {id} = query.parse(search)
    console.log(id);
    let history=useHistory()

    useEffect(() => {
        const fetchauthor = async () => {
            let result = await fetchAuthor()
            setAuthor(result)
        }
        fetchauthor()
    },[author])
    useEffect(()=>{
        const viewauth=async()=>{
            let result = await viewAuthor(id)
            setName(result.name)
            setEmai(result.email)
            setimageUrl(result.image)
        }
        viewauth()
    },[id])

    const onAdd=async(e)=>{
        if(search===""){
            e.preventDefault()
            let author={
                name,email
            }
            if(imageFile){
                let url=await uploadImage(imageFile)
                author.image=url
            }
        postAuthor(author).then(message=>alert(message))
        }else{
            e.preventDefault()
            let author={
                name,email
            }
            if(imageFile){
                let url=await uploadImage(imageFile)
                author.image=url
            }
            console.log("Article",author);
            console.log("image",author.image);
            
        updateAuthor(id,author).then(message=>alert(message))
        }
        const result = await fetchAuthor();
        setAuthor(result);
        setName("")
        setEmai("")
        history.push("/")
    }
    const onDelete=(id)=>{
        deleteAuthor(id).then((message)=>{
            alert(message)
            let tmp = author.filter(item=>{
                return item._id!==id
        })
        setAuthor(tmp)
    })
}
    return (
        <Container>
             <h2 className="my-2">Author</h2>
            <Row>
                <Col md="8">
                    <Form className="mt-4">
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Author Name</Form.Label>
                            <Form.Control onChange={(e)=>setName(e.target.value)} type="text" value={name} placeholder="Name" />
                            <Form.Text className="text-muted">
                                {/* text */}
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Email</Form.Label>
                            <Form.Control onChange={(e)=>setEmai(e.target.value)} type="email" value={email}  placeholder="Email" />
                        </Form.Group>

                        <Button onClick={onAdd} variant="primary" type="submit">
                        {search ? "Save": "Add"}
                        </Button>
                    </Form>
                </Col>

                <Col md="4">
                     <img className="w-100" src={imageUrl} />
                    <Form.Group controlId="formFile">
                        <Form.Label>Choose Image</Form.Label>
                        <Form.Control id="img" onChange={(e) => {
                            let url=URL.createObjectURL(e.target.files[0])
                            setimageFile(e.target.files[0])
                            url ? setimageUrl(url) : setimageUrl("https://designshack.net/wp-content/uploads/placeholder-image.png")
                            console.log("url",url);
                        }} 
                        type="file"
                        />
                    </Form.Group>
                </Col>
                <Col md="12" className="mt-3">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {author.map((item, index) => (
                            <tr>
                                <td>{item._id.slice(0,6)}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td><img style={{objectFit: 'cover',height:"70px"}} src={item.image ? item.image : "https://designshack.net/wp-content/uploads/placeholder-image.png"}/></td>
                                <td>
                                    <Button size="sm" onClick={()=>history.push(`/?id=${item._id}`)} variant="warning">Edit</Button> {' '}
                                    <Button size="sm" disabled={search ? true:false} onClick={()=>onDelete(item._id)} variant="danger">Remove</Button>
                                </td>
                            </tr>

                        ))}


                    </tbody>
                </Table>
            </Col>
            </Row>
           
        </Container>
    )
}
