import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Mymenu from './components/Mymenu';
import Pagenotfound from './components/Pagenotfound';
import Author from './views/Author';
export default function App() {
  return (
    <Router>
      <Mymenu/>
      <Switch> 
      <Route path='/' component={Author}/>
        <Route path='*' component={Pagenotfound}/>
      </Switch>
    </Router>
  )
}

