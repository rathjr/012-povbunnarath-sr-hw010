import api from '../Api/api'

export const fetchAuthor=async()=>{
    let result=await api.get('/author')
    return result.data.data
}

export const postAuthor=async(author)=>{
    let result= await api.post('/author',author)
    return result.data.message
}
export const uploadImage=async(file)=>{
    let formdata=new FormData()
    formdata.append('image',file)
    
    let result= await api.post('/images',formdata)
    return result.data.url
}
export const deleteAuthor=async(id)=>{
    let result=await api.delete('/author/'+id)
    return result.data.message
}

export const viewAuthor=async(id)=>{
    let result=await api.get('/author/'+id)
    return result.data.data
}

export const updateAuthor = async(id,author)=>{
    let result = await api.put('/author/'+id,author)
    return result.data.message
}
